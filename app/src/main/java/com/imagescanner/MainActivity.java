package com.imagescanner;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.imagescanner.utils.AppClass;
import com.imagescanner.utils.PermissionUtils;
import com.imagescanner.utils.SdcardUtils;
import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;

import java.io.IOException;


public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 99;
    private Button scanButton;
    private Button cameraButton;
    private Button mediaButton;
    private ImageView scannedImageView;
    private RecyclerView rcv_pdf_list;

    private final int
            EXTERNAL_STORAGE_CAMERA = 91;
    MediaAdapter
            mediaAdapter;
    ListView list_media;
    private String[] CAMERA_EXTERNALSTORAGE_PERMISSION = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    int media_icon[] = {android.R.drawable.ic_menu_camera, android.R.drawable.ic_menu_gallery};
    String media_name[] = {"Camera", "Gallery"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        /*initialise button*/
        scanButton = (Button) findViewById(R.id.scanButton);

        /*set scanButton click listener*/
        scanButton.setOnClickListener(new ScanButtonClickListener());

        /*initialise cameraButton*/
        cameraButton = (Button) findViewById(R.id.cameraButton);

        /*set cameraButton click listener*/
        cameraButton.setOnClickListener(new ScanButtonClickListener(ScanConstants.OPEN_CAMERA));

        /*initialise mediaButton*/
        mediaButton = (Button) findViewById(R.id.mediaButton);

        /*set mediaButton click listener*/
        mediaButton.setOnClickListener( new ScanButtonClickListener(ScanConstants.OPEN_MEDIA));


        scannedImageView = (ImageView) findViewById(R.id.scannedImage);

        /*initialise recyclerView*/
        rcv_pdf_list = (RecyclerView) findViewById(R.id.rcv_pdf_list);
        rcv_pdf_list.setLayoutManager(new LinearLayoutManager(MainActivity.this));
    }

    private class ScanButtonClickListener implements View.OnClickListener {

        private int preference;

        public ScanButtonClickListener(int preference) {
            this.preference = preference;
        }

        public ScanButtonClickListener() {
        }

        @Override
        public void onClick(View v) {
            if(isCameraStoragePermisson()) {
                startScan(preference);
            }
        }
    }

    protected void startScan(int preference) {
        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                getContentResolver().delete(uri, null, null);
                scannedImageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Bitmap convertByteArrayToBitmap(byte[] data) {
        return BitmapFactory.decodeByteArray(data, 0, data.length);
    }

    /**
     * To Check Permission
     */
    @TargetApi(Build.VERSION_CODES.M)
    private boolean isCameraStoragePermisson() {
        if (PermissionUtils.hasSelfPermission(MainActivity.this, CAMERA_EXTERNALSTORAGE_PERMISSION)) {
            return true;
        } else {
            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)
                    && shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
                    && shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                requestPermissions(CAMERA_EXTERNALSTORAGE_PERMISSION, EXTERNAL_STORAGE_CAMERA);
            } else {
                requestPermissions(CAMERA_EXTERNALSTORAGE_PERMISSION, EXTERNAL_STORAGE_CAMERA);
            }
        }
        return false;
    }


    /**
     * show Media Dialog For Pic Image
     */
    public void showMediaDialogForPicImage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        final LayoutInflater inflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.mediadialog, null);
        mediaAdapter = new MediaAdapter();
        list_media = (ListView) view.findViewById(R.id.list_media);
        list_media.setAdapter(mediaAdapter);
        builder.setTitle(R.string.selectphoto);
        builder.setView(view);

        // Create the AlertDialog
        final AlertDialog dialog = builder.create();

        list_media.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                switch (arg2) {
                    case 0:
                        SdcardUtils.MEDIA_FILE_ORIGINAL = SdcardUtils.returnImageFileName();
                        Log.v("", "SdcardUtils.MEDIA_FILE_ORIGINAL : " + SdcardUtils.MEDIA_FILE_ORIGINAL);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            // Do something for lollipop and above versions
                            SdcardUtils.CAMERA_IMAGE_URI = FileProvider.getUriForFile(MainActivity.this, MainActivity.this.getPackageName() + ".provider", SdcardUtils.MEDIA_FILE_ORIGINAL);

                        } else {
                            // do something for phones running an SDK before lollipop
                            SdcardUtils.CAMERA_IMAGE_URI = Uri.fromFile(SdcardUtils.MEDIA_FILE_ORIGINAL);
                        }

                        Log.v("", "imageuri " + SdcardUtils.CAMERA_IMAGE_URI);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, SdcardUtils.CAMERA_IMAGE_URI);
                        startActivityForResult(cameraIntent, SdcardUtils.ACTION_TAKE_PHOTO);
                        dialog.dismiss();
                        break;
                    case 1:
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(galleryIntent, SdcardUtils.ACTION_PICK_FROM_GALLERY);
                        dialog.dismiss();
                        break;
                }
            }
        });

        dialog.show();
    }

    /**
     * @param requestCode
     * @param permissions
     * @param grantResults Requesting Permission
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case EXTERNAL_STORAGE_CAMERA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED) {

                  //  showMediaDialogForPicImage();

                } else {
                    showAlertDialogToGetPermission();
                }
                break;
        }
    }

    /**
     * show Alert Dialog To Get Permission
     */
    private void showAlertDialogToGetPermission() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Hey " + "...");
        builder.setMessage(getResources().getString(R.string.allow_permission_settings));
        builder.setCancelable(false);
        builder.setPositiveButton("SETTING", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.setData(Uri.parse("package:" + MainActivity.this.getPackageName()));
                MainActivity.this.startActivity(intent);


            }
        });
        builder.show();
    }

    public class MediaAdapter extends BaseAdapter {

        final LayoutInflater inflater;

        public MediaAdapter() {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return media_icon.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = inflater.inflate(R.layout.mediadialog_row, parent, false);
                viewHolder.img_media = (ImageView) convertView.findViewById(R.id.img_media);
                viewHolder.txt_media_name = (TextView) convertView.findViewById(R.id.txt_media_name);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.img_media.setBackgroundResource(media_icon[position]);
            viewHolder.txt_media_name.setText(media_name[position]);
            viewHolder.txt_media_name.setTypeface(AppClass.parent_obj.typeface_Avenir_Regular, Typeface.BOLD);
            return convertView;
        }

        public class ViewHolder {

            TextView txt_media_name;
            ImageView img_media;
        }
    }

}
